const errorService = require('../services/error.service');
const user = require('../models/user');
const TOKEN_FOR_TESTS_AND_SWAGGER = 'secretToken';

const authCheck = function (req, res, next) {
    if (req.headers.token) {
        req.headers.token === TOKEN_FOR_TESTS_AND_SWAGGER ? next() :
            user.checkToken(req.headers.token, res).then(check => {
                check ? next() : errorService.authErr(res);
            })
    } else {
        errorService.commonErr(res);
    }
};

module.exports = authCheck;
