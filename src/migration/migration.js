const config = require('../config');
const knex = require('knex')(config);
const specialization = require('../models/specialization');
const employee = require('../models/employee');

const initialDB={
    specializations : ['frontend', 'backend', 'fullstack', 'project', 'manager', 'tester'],
    employee: {name: 'Vladimir', specializationId: 2}
};


const createSpecializations = () => {
    return knex.schema.createTable('specializations', (table) => {
        table.increments();
        table.string('specialization');
    });
};

const createEployee = () => {
    return knex.schema.createTable('employee', (table) => {
        table.increments();
        table.integer('specializationId').unsigned().references('id').inTable('specializations');
        table.string('name');
    })
};

const createProject = () => {
    return knex.schema.createTable('projects', (table) => {
        table.increments();
        table.string('name');
        table.string('techStack');
    })
};

const createProjectEmploee = () => {
    return knex.schema.createTable('projects_employee', (table) => {
        table.increments();
        table.integer('employeeId').unsigned().references('id').inTable('employee');
        table.integer('projectsId').unsigned().references('id').inTable('projects');
    })
};

const createUsers = () => {
    return knex.schema.createTable('users',  (table) => {
        table.increments();
        table.string('login');
        table.string('password');
        table.string('token');
    })
};

const fillSpecializations = () => {
    initialDB.specializations.map(el => {
        return specialization.insert({specialization: el}).then(console.log);
    });
};

const fillEmployee = () => {
    return employee.insert(initialDB.employee).then(console.log);
};

const migrate = () => {
    createSpecializations()
        .then(() => createEployee())
        .then(() => createProject())
        .then(() => createProjectEmploee())
        .then(() => createSpecializations())
        .then(() => createUsers())
        .then(() => {
            fillEmployee();
            fillSpecializations();
            console.log("Migrate is over")
        })
        .catch(err => console.log(err.message));
};

migrate();

