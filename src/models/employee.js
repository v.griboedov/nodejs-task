const model = require('./model');
const TABLE = 'employee';

class Employee extends model {
    constructor() {
        super(TABLE);
    }

}

module.exports = new Employee();
