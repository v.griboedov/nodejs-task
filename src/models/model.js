const config = require('../config');
const knex = require('knex')(config);
const PRIMERY_KEY = 'id';

class Model {
    constructor(tableName) {
        this.tableName = tableName;
    }

    getById(id) {
        return knex(this.tableName).where(PRIMERY_KEY, id);
    }

    getAll() {
        return knex.select().table(this.tableName);
    }

    find(where) {
        return knex.select('*').from(this.tableName).where(where);
    }

    insert(inserted) {
        return knex(this.tableName).insert(inserted);
    }

    deleteById(id) {
        return this.getById(id).del();
    }

    update(id, params) {
        return knex(this.tableName).where(PRIMERY_KEY, '=', id).update(params);
    }

}

module.exports = Model;
