const model = require('./model');
const TABLE = 'projects';

class Project extends model {
    constructor() {
        super(TABLE);
    }

}

module.exports = new Project();
