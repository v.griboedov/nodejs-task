const model = require('./model');
const TABLE = 'specializations';

class Specialization extends model {
    constructor(){
        super(TABLE);
    }
}

module.exports = new Specialization();
