const model = require('./model');
const jwt = require('jwt-simple');
const CODE = 'secret';
const TABLE = 'users';

class User extends model {
    constructor() {
        super(TABLE);
    }

    async authorizate(login, password) {
        const resp = await this.find({login, password});
        return resp[0] && resp[0].token === jwt.encode({login: login}, CODE) ? resp[0].token : null;
    }

    async checkToken(token) {
        const user = await this.find({token});
        return !!(user[0] && jwt.decode(token, CODE).login.trim() === user[0].login.trim());
    }

    addUser(userParams) {
        const token = jwt.encode({login: userParams.login}, CODE);
        return this.insert({...userParams, token: token}).then(() => token);
    }

}

module.exports = new User();
