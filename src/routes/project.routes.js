const projectRoutes = require('../models/project');
const authCheck = require('../middleware/authCheck');
const sendService = require('../services/send.service');
const express = require('express');
const router = express.Router();


module.exports =  (app) => {
    app.use('/project', router);

    router.get('/:id', authCheck, (req, res) => {
        projectRoutes.getById(req.params.id).then(project => {
            sendService.send(res, project);
        });
    });

    router.get('', authCheck, (req, res) => {
        projectRoutes.getAll(req.params.id).then(projects => {
            sendService.send(res, projects);
        });
    });

    router.post('', authCheck, (req, res) => {
        projectRoutes.insert({
            ...req.body
        }).then(id => {
            sendService.send(res, id);
        })
    });

    router.delete('/:id', authCheck, (req, res) => {
        projectRoutes.deleteById(req.params.id).then(project => {
            sendService.send(res, project)
        });
    });

    router.put('/:id', authCheck, (req, res) => {
        projectRoutes.update(req.params.id, req.body).then(project => {
            sendService.send(res, project)
        });
    });

};
