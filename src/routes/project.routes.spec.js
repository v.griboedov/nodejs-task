const request = require('supertest');

module.exports = (app) => {
    const headers = {'Accept': 'application/json', 'token': 'secretToken'};

    describe('GET /project', () => {
        it('respond with json containing a list of all projects', done => {
            request(app)
                .get('/project')
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });
    });

    describe('GET /project/:id', () => {
        it('respond with json containing project chosen by id', done => {
            request(app)
                .get('/project/1')
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });
    });


    describe('PUT /project/:id', () => {
        const data = {
            "id": 1,
            "name": "name",
            "techStack": "techStack",
        };

        it('respond with updated project', done => {
            request(app)
                .put('/project/1')
                .send(data)
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });

    });

    describe('DELETE /project/:id', () => {
        it('respond with deleted project', done => {
            request(app)
                .delete('/project/1')
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });

    });

    describe('POST /project', () => {
        const data = {
            "name": "name",
            "techStack": "techStack",
        };

        it('respond with project', done => {
            request(app)
                .post('/project')
                .send(data)
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });
    });
};
