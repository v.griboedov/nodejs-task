const user = require('../models/user');
const authCheck = require('../middleware/authCheck');
const sendService = require('../services/send.service');
const express = require('express');
const router = express.Router();
const errorService = require('../services/error.service');


module.exports = function (app) {
    app.use('/user', router);

    router.get('/:id', authCheck, (req, res) => {
        user.getById(req.params.id).then(user => {
            sendService.send(res, user);
        });
    });

    router.get('', authCheck, (req, res) => {
        user.getAll(req.params.id).then(users => {
            sendService.send(res, users);
        });
    });

    router.post('', (req, res) => {
        user.addUser({
            login: req.body.login,
            password: req.body.password,
        }).then(token => {
            sendService.send(res, token);
        })
    });

    router.delete('/:id', authCheck, (req, res) => {
        user.deleteById(req.params.id).then(user => {
            sendService.send(res, user)
        });
    });

    router.put('/:id', authCheck, (req, res) => {
        user.update(req.body.id, req.body).then(update => {
            sendService.send(res, update);
        });
    });

    router.post('/authorize', (req, res) => {
        user.authorizate(req.body.login, req.body.password).then(token => {
            if(token) {
                sendService.send(res, token)
            }else{
                errorService.commonErr(res);
            }
        });
    });
};
