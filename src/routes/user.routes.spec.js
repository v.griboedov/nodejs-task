const request = require('supertest');
const jwt = require('jwt-simple');


module.exports = (app) => {

    const headers = {'Accept': 'application/json', 'token': 'secretToken'};
    const data = {
        "login": "test@mail.com",
        "password": "123456789",
    };
    const badData = {
        "login": "badLogin",
        "password": "badPass",
    };

    describe('GET /user', () => {
        it('respond with json containing a list of all users', done => {
            request(app)
                .get('/user')
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });

        it('request without token, respond 500 ', done => {
            request(app)
                .get('/user')
                .set({})
                .expect('Content-Type', "text/plain; charset=utf-8")
                .expect(500, done);
        });

        it('request with bad token, respond 401 ', done => {
            request(app)
                .get('/user')
                .set({'Accept': 'application/json', 'token': 'badToken'})
                .expect('Content-Type', "text/plain; charset=utf-8")
                .expect(401, done);
        });

        it('request with bad token (bad secretCode), respond 401 ', done => {

            const token = jwt.encode({login: badData.login}, 'badSecretCode');
            request(app)
                .get('/user')
                .set({'Accept': 'application/json', 'token': token})
                .expect('Content-Type', "text/plain; charset=utf-8")
                .expect(401, done);
        });
    });


    describe('GET /user/:id', () => {
        it('respond with json containing user chosen by id', done => {
            request(app)
                .get('/user/11')
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });
    });

    describe('PUT /user/:id', () => {
        const data = {
            "id": 11,
            "login": "test@mail.com",
            "password": "123456789",
        };

        it('respond with updated user', done => {
            request(app)
                .put('/user/11')
                .send(data)
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });

    });

    describe('DELETE /user/:id', () => {
        it('respond with deleted user', done => {
            request(app)
                .delete('/user/11')
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });

    });

    describe('POST /user', () => {

        it('respond with token', done => {
            request(app)
                .post('/user')
                .send(data)
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });
    });

    describe('POST /authorize', () => {

        it('respond with token', done => {
            request(app)
                .post('/user/authorize')
                .send(data)
                .set(headers)
                .expect('Content-Type', "text/html; charset=utf-8")
                .expect(200, done);
        });

        it('rwquest with badData, respond with 500', done => {
            request(app)
                .post('/user/authorize')
                .send(badData)
                .set(headers)
                .expect('Content-Type', "text/plain; charset=utf-8")
                .expect(500, done);
        });
    });
};
