class ErrorHandler {
    constructor() {
    }

    authErr(res) {
        res.sendStatus(401);
    }

    commonErr(res) {
        res.sendStatus(500)
    }

}

module.exports = new ErrorHandler();
