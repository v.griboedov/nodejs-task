class SendService {
    constructor() {
    }

    send(res, payload) {
    res.send(JSON.stringify(payload));
    }

}

module.exports = new SendService();
